# About 

arXivOt is an app (bot) that unfurls (adds the message attachment) to the posts on slack from arXiv links.

This project is an extension on previous projects:
* https://github.com/rshin/arxiv-slack-bot
* https://github.com/joebullard/slack-arxivbot
* https://github.com/slackapi/sample-app-unfurls

I have a small node running it, in case you are interested in using it.  (No promisses on the support, though.)

<a href="https://slack.com/oauth/authorize?scope=incoming-webhook&client_id=179551653108.366916335671"><img alt="Add to Slack" height="40" width="139" src="https://platform.slack-edge.com/img/add_to_slack.png" srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x" /></a>

# Deploy on your own machine

## On your server
* Install [node, npm, and pm2](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04) or similars to deploy.
* Configure your certificates, [let's encrypt is an option](https://letsencrypt.org/) and make them readable and accessible for the script.

## On Slack
* Create a new Slack app in your Slack team
* Under Basic Information on the settings of the app, get the credentials
  * `Verification Token`, and store it in a `.env` file as `SLACK_VERIFICATION_TOKEN`
* Under Event Subscriptiptions:
  1. Test your URL and port settings for node by using the [slack api](https://github.com/slackapi/node-slack-events-api#configuration), and your URL should be verified.  (Your URL should be something like `https://your.domain.com/slack/events`.)
  2. Enable `link_shared` on Subscribe to Workspace Events
  3. Add `arxiv.org` to App Unfurl Domains.
* Under OAuth & Permissions:
    1. Set the Redirect URL and point it to `https://your.domain.com/auth`.
    2. On Scopes, add the `links:write` permission scope.
* Install the app with your account in your Slack team.

## On your server

* You should have a `.env` file with

```
SLACK_VERIFICATION_TOKEN=xxx
SLACK_CLIENT_TOKEN=xoxp-xxx
SLACK_APP_ID=xx
SLACK_CLIENT_ID=xxx
SLACK_CLIENT_SECRET=xx

DB=./db

PRIVATE_KEY=/path/to/private/key
CERTIFICATE=/path/to/certificate
```

* Install the dependencies with
```bash
cd arxivot
npm install
```
* Add the bot to the `pm2`
```bash
pm2 start arxivot.js
```
