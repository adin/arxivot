
// Tokens need to be set on .env, like
// SLACK_VERIFICATION_TOKEN=xxx
// SLACK_CLIENT_TOKEN=xoxp-xxxx
require('dotenv').config();

// My server
var fs = require('fs');
var https = require('https');

var privateKey  = fs.readFileSync(process.env.PRIVATE_KEY, 'utf8');
var certificate = fs.readFileSync(process.env.CERTIFICATE, 'utf8');

var credentials = {key: privateKey, cert: certificate};

// Initialize using verification token from environment variables
// See: https://github.com/slackapi/node-slack-events-api
const slackEventsAPI = require('@slack/events-api');
const createSlackEventAdapter = slackEventsAPI.createSlackEventAdapter;
const slackEvents = createSlackEventAdapter(process.env.SLACK_VERIFICATION_TOKEN, {includeBody: true});
const port = process.env.PORT || 3000;

// Initialize an Express application
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Initialize arxiv stuff
const keyBy = require('lodash.keyby');
const omit = require('lodash.omit');
const mapValues = require('lodash.mapvalues');
const normalizePort = require('normalize-port');

const {promisify} = require('es6-promisify');
var rp = require('request-promise');
var parseString = promisify(require('xml2js').parseString);

const ARXIV_ID   = /\d{4}\.\d{4,5}/;
const ARXIV_LINK = /(?:https?:\/\/)?arxiv\.org\/(?:abs|pdf)\/(\d{4}\.\d{4,5})(?:v\d+)?(?:.pdf)?/g;
const ARXIV_API_URL = 'http://export.arxiv.org/api/query?search_query=id:';


/**
 * Gets the data from the arXiv export site
 *
 * @param {string} arxivId - the extracted ID from the link
 *
 * @returns {Object} parsedBody - JSON of the interesting fields
 *
 * See:  parseApiResponseBody
 */
const fetchArxiv = function (arxivId, callback) {
  return rp(ARXIV_API_URL + arxivId).then(parseApiResponseBody);
};


/**
 * Transforms the XML from arXiv API into a JSON data set.
 *
 * @param {Object} body - XML response
 *
 * @returns {Object} parsedBody - JSON of the interesting fields
 */
const parseApiResponseBody = function (body) {
  return parseString(body).then(result => {
    if (!result.feed.entry) {
      throw new Error('ArXiv entry not found');
    }
    var entry = result.feed.entry[0];
    return {
      id      : entry.id ?
                entry.id[0].split('/').pop() :
                '{No ID}',
      url     : entry.id ?
                entry.id[0] :
                '{No url}',
      title   : entry.title ?
                entry.title[0].trim().replace(/\n/g, ' ') :
                '{No title}',
      summary : entry.summary ?
                entry.summary[0].trim().replace(/\n/g, ' ') :
                '{No summary}',
      authors : entry.author ?
                entry.author.map(function (a) { return a.name[0]; }) :
                '{No authors}',
      categories : entry.category ? entry.category.map(c => c.$.term) : [],
      updated_time : Date.parse(entry.updated) / 1000,
    };
  });
}

/**
 * Transform a Slack arXiv link into a Slack message attachment.
 *
 * @param {Object} link - Slack link
 * @param {string} link.url - The URL of the link
 *
 * @returns {Promise.<Object>} An object described by the Slack message attachment structure. In
 * addition to the properties described in the API documentation, an additional `url` property is
 * defined so the source of the attachment is captured.
 * See: https://api.slack.com/docs/message-attachments
 * Adapted from: https://github.com/slackapi/sample-app-unfurls and https://github.com/rshin/arxiv-slack-bot/blob/master/index.js
 */
function messageAttachmentFromLink(link) {
  return fetchArxiv(link.url.match(ARXIV_ID)[0])
    .then((arxivData) => {
        const attachment = {
          author_name: arxivData.authors.join(', '),
          title      : '[' + arxivData.id + '] ' + arxivData.title,
          title_link : arxivData.url,
          text       : arxivData.summary,
          footer     : arxivData.categories.join(', '),
          footer_icon: 'https://arxiv.org/favicon.ico',
          ts         : arxivData.updated_time,
          color      : '#b31b1b',
          url        : link.url,
        };
        return attachment;
      });
}

// I am using this to store tokens quickly for this demo, but you probably want to use a real DB!
const storage = require('node-persist'); 
storage.init({dir: process.env.DB, logging: true, });

// Create the app and set all the listeners
// You must use a body parser for JSON before mounting the adapter
app.use(bodyParser.json());

// Mount the event handler on a route
// NOTE: you must mount to a path that matches the Request URL that was configured earlier
app.use('/slack/events', slackEvents.expressMiddleware());


// Handle the event from the Slack Events API
slackEvents.on('link_shared', (event, body) => {
  // Call a helper that transforms the URL into a promise for an attachment suitable for Slack
  Promise.all(event.links.map(messageAttachmentFromLink))
    // Transform the array of attachments to an unfurls object keyed by URL
    .then(attachments => keyBy(attachments, 'url'))
    .then(unfurls => mapValues(unfurls, attachment => omit(attachment, 'url')))
    // Invoke the Slack Web API to append the attachment
    .then(unfurls => {
      storage.getItem(body.team_id)
      .then(oauth =>{
        return rp.post({
          url: 'https://slack.com/api/chat.unfurl',
          form: {
            token: oauth,
            channel: event.channel,
            ts: event.message_ts,
            unfurls: JSON.stringify(unfurls)
          },
        })
      })
    })
    .catch(console.error);
});

// Handle errors
const slackEventsErrorCodes = slackEventsAPI.errorCodes;
slackEvents.on('error', (error) => {
  if (error.code === slackEventsErrorCodes.TOKEN_VERIFICATION_FAILURE) {
    console.warn(`An unverified request was sent to the Slack events request URL: ${error.body}`);
  } else {
    console.error(error);
  }
});

// OAuth Stuff
app.get('/auth', function(req, res){
  if (!req.query.code) { // access denied
    console.error('Access denied');
    return;
  }
  rp.post({
    url: 'https://slack.com/api/oauth.access',
    form: {
      client_id: process.env.SLACK_CLIENT_ID,
      client_secret: process.env.SLACK_CLIENT_SECRET,
      code: req.query.code
    },
  })
  .then(response => {
    if (!JSON.parse(response).ok)
      console.error(JSON.parse(response).error);
    else {
      // Get an auth token (and store the team_id / token)
      storage.setItem(JSON.parse(response).team_id, JSON.parse(response).access_token);
      
      // Redirect to their page on success
      res.redirect('https://slack.com/app_redirect?app='+ process.env.SLACK_APP_ID);
    }
  })
  .catch(err => console.error(err));
});


// Start the express application
https.createServer(credentials, app).listen(port, () => {
  console.log(`server listening on port ${port}`);
});
